# nvim setup

Fetch latest neovim repo, compile and install locally.
Deploy config files using stow.

## Usage
    ./dependencies.sh && ./setup.sh
