#!/bin/bash

set -o nounset
set -e

function status() {
	echo "=== neovim dependencies ===" "$1"
}

function tryDepDebianlike() {
	status "Trying debian-like install..."
	which apt-get && true
	if (( $? == 0 )); then
		sudo apt-get install ninja-build gettext cmake unzip curl stow
		return 0
	else
		status "INFO: Not debian-like (apt-get not found)"
		return 1
	fi
}

tryDepDebianlike

status "OK: neovim dependencies installed"
